#include "Window.h"

int main(){
    Window window(1280, 720, "6502");
    
    while(!window.ShouldClose()){
        window.Clear(0, 0.18039215686f, 1, 1);
        window.SwapBuffers();
    }
}