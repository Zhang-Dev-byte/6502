#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <memory>
#include <string>

class Window{
public:
    Window() = default;
    Window(int width, int height, const std::string& title);
    bool ShouldClose();
    void SwapBuffers();
    void Clear(float r, float g, float b, float a);
    ~Window();
    static void ResizeCallback(GLFWwindow* window, int width, int height);
private:
    GLFWwindow* m_handle;
    int width, height;
    std::string title;
};