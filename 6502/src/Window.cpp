#include "Window.h"
#include <cassert>

void Window::ResizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

Window::Window(int width, int height, const std::string& title)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    this->width = width;
    this->height = height;
    this->title = title;

    this->m_handle = glfwCreateWindow(this->width, this->height, this->title.c_str(), NULL, NULL);
    
    assert(this->m_handle);
    glfwMakeContextCurrent(this->m_handle);
    assert(gladLoadGLLoader((GLADloadproc)glfwGetProcAddress));
    glViewport(0, 0, this->width, this->height);

    glfwSetFramebufferSizeCallback(this->m_handle, Window::ResizeCallback);  
}
Window::~Window()
{
    delete m_handle;
}

bool Window::ShouldClose()
{
    return glfwWindowShouldClose(this->m_handle);
}
void Window::SwapBuffers()
{
    glfwSwapBuffers(this->m_handle);
    glfwPollEvents();
}

void Window::Clear(float r, float g, float b, float a)
{
    glClearColor(r, g, b, a);
    glClear(GL_COLOR_BUFFER_BIT);
}